# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from mpl_toolkits.mplot3d import Axes3D

#change the default matplotlib font, that doesn't print the utf-8 µ !
#the 'well-known' latex definition $\mu$ works perfect in any case
#ref :http://stackoverflow.com/questions/10960463/non-ascii-characters-in-matplotlib
#plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})

# Рисовальщик решения ОДУ и СДУ
def plot_solution(xred, equation_type='ode', step=0.01, x_0=[1.0, 0.0, 0.0]):
    """Функция строит графики решений и фазовый портрет для ОДУ
    полученного вышеопределенным методом 'solve_ode()'.

    Аргументы:
        equation_type: тип уравнения,
        step: шаг,
        x_0: начальная значение.
    """

    if equation_type == 'ode':
        t, W, Q, Qe = xred.solve_ode(step, x_0)
    elif equation_type == 'sde':
        t, W, Q, Qe = xred.solve_sde(step, x_0)

    fig, ax = plt.subplots(nrows=3, ncols=1)
    frmt = "($q_{{min}}={0:d},\;q_{{max}}={1}$)".format(int(100*xred.q_min), int(100*xred.q_max))
    label_1 = r"Размер TCP-окна "+frmt
    label_2 = r"Мгновенный размер очереди "+frmt
    label_3 = "Экспоненциально взвешенное скользящее \nсреднее от $Q(t)$ "+frmt

    #ax[0].set_title()
    #ax[1].set_title()
    #ax[2].set_title()

    ax[0].set_ylabel("$W(t)$ [пак.]", fontsize=14)
    ax[1].set_ylabel("$Q(t)$ [пак.]", fontsize=14)
    ax[2].set_ylabel("$\hat{Q}(t)$ [пак.]", fontsize=14)

    ax[0].set_xticklabels([])
    ax[1].set_xticklabels([])

    for a in ax:
        a.set_ylim([0, 100])
        a.yaxis.tick_right()
        a.grid(lw=0.2, linestyle=':')

    ax[2].set_xlabel(r"Время [сек.]")

    # Рисуем отдельные графики решений на одном полотне
    ax[0].plot(t, W, lw=0.75, color='k', label=label_1)
    ax[1].plot(t, Q, lw=0.75, color='k', label=label_2)
    ax[2].plot(t, Qe, lw=0.75, color='k', label=label_3)

    for a in ax:
        a.legend(frameon=False)

    # -------------------------------------------------------------------------
    # Рисуем фазовый портрет
    fig_3d = plt.figure(100)
    ax_3d = fig_3d.add_subplot(1, 1, 1, projection='3d')

    ax_3d.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax_3d.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax_3d.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))

    ax_3d.w_xaxis.gridlines.set_lw(0.3)
    ax_3d.w_yaxis.gridlines.set_lw(0.3)
    ax_3d.w_zaxis.gridlines.set_lw(0.3)
    tl = r"Фазовый портрет дисциплины {0}. $q_{{min}}={1}$, $q_{{max}} = {2}$"
    ax_3d.set_title(tl.format(xred.name, 100*xred.q_min, 100*xred.q_max))
    ax_3d.set_xlabel("$W(t)$ [пак.]", fontsize=14)
    ax_3d.set_ylabel("$Q(t)$ [пак.]", fontsize=14)
    ax_3d.set_zlabel("$\hat{Q}(t)$ [пак.]", fontsize=14)

    ax_3d.plot(W, Q, Qe, color='k', lw=0.5)

    return (fig, fig_3d)

# Рисовальщик спектра решений ОДУ и СДУ
def plot_solution_spectrum(xred, equation_type='ode', step=0.01, x_0=[1.0, 0.0, 0.0]):
    import numpy as np
    """Функция строит графики спектра решений ОДУ или СДУ
    полученного вышеопределенным методом 'solve_ode()'.

    Аргументы:
        equation_type: тип уравнения,
        step: шаг,
        x_0: начальная значение.
    """

    if equation_type == 'ode':
        t, W, Q, Qe = xred.solve_ode(step, x_0)
    elif equation_type == 'sde':
        t, W, Q, Qe = xred.solve_sde(step, x_0)

    fig, ax = plt.subplots(nrows=3, ncols=1)

    for a in ax:
        a.grid(which = 'major', color='gray', linestyle='dashed', linewidth=1)

    frmt = "($q_{{min}}={0:d},\;q_{{max}}={1}$)".format(int(100*xred.q_min), int(100*xred.q_max))
    label_1 = r"Размер TCP-окна "+frmt
    label_2 = r"Мгновенный размер очереди "+frmt
    label_3 = "Экспоненциально взвешенное скользящее \nсреднее от $Q(t)$ "+frmt

    #ax[0].set_title()
    #ax[1].set_title()
    #ax[2].set_title()

    ax[0].set_ylabel("Спектр $W(t)$", fontsize=14)
    ax[1].set_ylabel("Спектр $Q(t)$", fontsize=14)
    ax[2].set_ylabel("Спектр $\hat{Q}(t)$", fontsize=14)

    ax[0].set_xticklabels([])
    ax[1].set_xticklabels([])

    for a in ax:
        #a.set_ylim([0, 0.1])
        a.set_yscale('log')
        a.yaxis.tick_right()
        a.grid(lw=0.2, linestyle=':')

    ax[2].set_xlabel("Частота [Гц]")

    # Вычисляем спектр
    spectrum_W = np.abs(np.fft.fft(W))
    spectrum_Q = np.abs(np.fft.fft(Q))
    spectrum_Qe = np.abs(np.fft.fft(Qe))


    # Нормировка (нужна ли?)
#    spectrum_W /= len(spectrum_W)*0.25
#    spectrum_Q /= len(spectrum_Q)*0.25
#    spectrum_Qe /= len(spectrum_Qe)*0.25

    # Находим частоты
    f_W = np.fft.fftfreq(W.size, step)
    f_Q = np.fft.fftfreq(Q.size, step)
    f_Qe = np.fft.fftfreq(Qe.size, step)

    # Сортируем и возвращаем последовательность сортированных индексов
    id_W = np.argsort(f_W[f_W > 0.0])
    id_Q = np.argsort(f_Q[f_Q > 0.0])
    id_Qe = np.argsort(f_Qe[f_Qe > 0.0])

    # Рисуем отдельные графики решений на одном полотне
    ax[0].plot(f_W[id_W], spectrum_W[id_W], lw=0.2, color='k', label=label_1)
    ax[1].plot(f_Q[id_Q], spectrum_Q[id_Q], lw=0.2, color='k', label=label_2)
    ax[2].plot(f_Qe[id_Qe], spectrum_Qe[id_Qe], lw=0.2, color='k', label=label_3)

    for a in ax:
        a.legend(frameon=False)

    return fig


def __error_bars_calculation(xred, W, Q):
    """Считаем величину матрицы диффузии"""
    # Мне вообще-то не очень ясно что это дает
    W_error = []
    Q_error = []

    # Будем портить атрибут q_max поэтому его надо сохранить
    #q_old = xred.q_max

    for w, q in zip(W, Q):
        #xred.q_max = qmax
        ws, qs = xred.sde_G([w, q, 0]).diagonal()[0:2]
        W_error.append(ws)
        Q_error.append(qs)

    #xred.q_max = q_old

    return W_error, Q_error


def __error_bars_plot(x, y, dx, dy, angle):
    """Функция рисует функция рисует планки погрешносей под указанным углом
    к оси абсцисс.

    Аргументы:
        x, y --- массивы коорддинат;
        dx, dy --- массивы погрешностей;
        angle --- угол в градусах.
    Функция возвращает коллекцию линий.
    """
    import numpy as np
    a = angle*np.pi/180

    x_1 = np.asarray(x) + np.asarray(dx)*np.cos(a)
    y_1 = np.asarray(y) + np.asarray(dy)*np.sin(a)
    x_2 = np.asarray(x) - np.asarray(dx)*np.cos(a)
    y_2 = np.asarray(y) - np.asarray(dy)*np.sin(a)

#    v = np.array([x_1-x_2, y_1-y_2]).T
#    r = np.linalg.norm(v, axis=1)

    # длина засечки
    k = 0.05

    # Координаты концов верхней и нижней засечек
    # непропорциональность осей портит все вычисления !!
#    tik1_x_1 = x_1 + k*si
#    tik1_x_2 = x_1 - k*si
#    tik1_y_1 = y_1 - k*co
#    tik1_y_2 = y_1 + k*co
#
#    tik2_x_1 = x_2 + k*si
#    tik2_x_2 = x_2 - k*si
#    tik2_y_1 = y_2 - k*co
#    tik2_y_2 = y_2 + k*co

#    tik1 = zip(tik1_x_1, tik1_x_2, tik1_y_1, tik1_y_2)
#    tik2 = zip(tik2_x_1, tik2_x_2, tik2_y_1, tik2_y_2)

    # Собираем все пары точек в один список
    ls = [[(v1, v3), (v2, v4)] for v1, v2, v3, v4 in zip(x_1, x_2, y_1, y_2)]
#    ls += [[(v1, v3), (v2, v4)] for v1, v2, v3, v4 in tik1]
#    ls += [[(v1, v3), (v2, v4)] for v1, v2, v3, v4 in tik2]

    return ls

def plot_self_oscillation_solution_2d(xred, equation_type='ode'):
    """Рисуем зависимости q(q_min) и q(q_min, q_max)"""

    sde = True if equation_type == 'sde' else False

    q_max, W, Q = xred.solve_self_oscillation_equation_2d()

    fig_2d = plt.figure(0)
    ax_2d = fig_2d.add_subplot(1, 1, 1)

    title = r"Параметры $q_{{min}}={0}$, $q_{{max}} = {1}$"
    title = title.format(100*xred.q_min, 100*xred.q_max)
    #ax_2d.set_title(title)
    ax_2d.set_xlabel(r"$q_{max}$")
    ax_2d.set_ylabel(r"$Q(q_{max})$")

    ax_2d.plot(100.0*q_max, Q, color='k', lw=0.75, label=r'$Q(q_{max})$')
    ax_2d.plot(100.0*q_max, 100.0*q_max, color='k', lw=0.75, marker='o',
       markersize=3, markevery=5, label=r'$Q = q_{max}$. ' + title)

    # Для стохастического случая
    #write(*,*) q_max, y, y(1) + Ws(y,P), y(1) - Ws(y,P), y(2) + Qs(y), y(2)-Qs(y)

    if sde is True:
        W_error, Q_error = __error_bars_calculation(xred, W, Q)
        # Делаем так, чтобы узы не залезали в отрицательную область
        Q_err = [q_err if q-q_err>0 else q-1.0 for (q, q_err) in zip(Q, Q_error)]

        lc = __error_bars_plot(100*q_max, Q, Q_err, Q_error, 45)
        ax_2d.add_collection(LineCollection(lc[4::3],
                                            colors='gray', linewidths=0.75))

        #ax_2d.plot(100*q_max[::5], Q[::5], ls='None', marker='o', markersize=1)

    ax_2d.legend(loc='upper left', ncol=2)

    return fig_2d


def plot_self_oscillation_solution_3d(xred, equation_type='ode'):
    """Рисуем трехмерный график с использованием gnuplot, так как matplotlib
    позорно с этой задачей не справляется и не умеет граммотно пересекать
    поверхности"""
    import os
    import csv
    import subprocess

    from sys import executable

    sde = True if equation_type == 'sde' else False

    q_grid, W, Q = xred.solve_self_oscillation_equation_3d()

    if sde is True:
        W_error, Q_error = __error_bars_calculation(xred, W, Q)

    file = 'data.csv'
    f = open(file, 'wt')
    try:
        writer = csv.writer(f)
        if sde is False:
            writer.writerow(('q_min', 'q_max', 'W', 'Q'))
            for qm, w, q in zip(q_grid, W, Q):
                writer.writerow((qm[0], qm[1], w, q))
        elif sde is True:
            writer.writerow(('q_min', 'q_max', 'W', 'Q', 'W_error', 'Q_error'))
            for qm, w, q, werr, qerr in zip(q_grid, W, Q, W_error, Q_error):
                writer.writerow((qm[0], qm[1], w, q, werr, qerr))
    finally:
        f.close()
    # Букву диска узнаем автоматически
    gnuplot_win = "{}:/WinPython/gnuplot/bin/gnuplot.exe".format(executable[0])
    gnuplot_linux = "gnuplot"
    gnuplot = gnuplot_win if os.name == 'nt' else gnuplot_linux
    if sde is False:
        cmd = [gnuplot, "-e",
               "model_name='{0}'".format(xred.name), "plot.gp"]
    elif sde is True:
        cmd = [gnuplot, "-e",
               "model_name='{0}'; sde='1'".format(xred.name), "plot.gp"]

    print(" ".join(cmd))
    subprocess.call(cmd)

    if sde is False:
        return "{0}_3d.pdf".format(xred.name)
    elif sde is True:
        return "{0}_stochastic_3d.pdf".format(xred.name)
