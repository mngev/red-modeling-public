# -*- coding: utf-8 -*-
"""В данном файле несколько функций для обработки конфигурационных файлов"""


def create_xred(xred, init_dictionaries):
    """Создает объекты RED и подклассов по названию и по инициализирующему
    словарю"""
    xreds = []
    for name, init_dictionary in init_dictionaries.items():
        if str(name).startswith('RED'):
            qd = xred.RED(init_dictionary)
            qd.name = name
            xreds.append(qd)
        elif str(name).startswith('EFRED'):
            qd = xred.EFRED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        elif str(name).startswith('ARED'):
            qd = xred.ARED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        elif str(name).startswith('POWARED'):
            qd = xred.POWARED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        elif str(name).startswith('AURED'):
            qd = xred.AURED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        elif str(name).startswith('RARED'):
            qd = xred.RARED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        elif str(name).startswith('WRED'):
            qd = xred.WRED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        elif str(name).startswith('SARED'):
            qd = xred.SARED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        elif str(name).startswith('DSRED'):
            qd = xred.DSRED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        elif str(name).startswith('GRED'):
            qd = xred.GRED(init_dictionary)
            qd.name = name
            xreds.append(qd)

        else:
            print("Такой дисциплины обслуживания нет!")
            xreds.append(xred.RED())
    return xreds


def read_config(config_files_names):
    """Функция считывает конфигурационные ini файлы и возвращает ConfigParser
    объект для дальнейшей обработки"""
    import configparser

    config = configparser.ConfigParser()
    config.optionxform = str

    for config_file_name in config_files_names:
        try:
            file = open(config_file_name, 'rt', encoding='utf-8')
            config.read_file(file)
        except FileNotFoundError:
            print("Файл {} не найден и игнорируется".format(config_file_name))

    return config


def read_qd_config(config_files_names):
    """Функция считывает конфигурационный файл, в котором записаны параметры
    дисциплин обслуживания. На основе этого файла формируется словарь словарей,
    на основании которых конструктор класса RED (+подклассов) создает объект"""

    # В этот словарь собираются в виде подсловарей параметры из ini файла.

    config = read_config(config_files_names)

    config_dictionary = {}

    for section in config.sections():
        keys, values = [], []
        for key, value in config[section].items():
            keys.append(key)
            if key in ['N', 'c']:
                values.append(int(value))
            elif key == 'with_pto':
                values.append(config[section].getboolean(key))
            else:
                values.append(float(value))
        config_dictionary[section] = dict(zip(keys, values))

    return config_dictionary


def read_calculations_config(config_files_names):
    """Функция принимает список конфигурационных файлов, в которых записаны
    настройки вычислений. На основе этого файла формируется словарь, который
    дальше используется для управления вычислениями"""

    config = read_config(config_files_names)

    config_dictionary = {}

    for section in config.sections():
        keys, values = [], []

        if section == 'Параметры метода':
            for key, value in config[section].items():
                keys.append(key)
                values.append(float(value))
            config_dictionary['Method_options'] = dict(zip(keys, values))

        if section == 'Прочие настройки':
            for key, value in config[section].items():
                keys.append(key)
                values.append(value)
            config_dictionary['Other_options'] = dict(zip(keys, values))

        if section == 'Типы вычислений':
            for key in config[section].keys():
                keys.append(key)
                values.append(config[section].getboolean(key))
            config_dictionary['Calculations'] = dict(zip(keys, values))

    return config_dictionary