#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
import matplotlib.pyplot as plt

import red
import plot
from settings import *

# Настройки
settings_path = 'settings' + os.path.sep

calc_settings = ['параметры вычислений.ini']
# 'параметры дисциплин.ini',
qd_settings = ['параметры дисциплин.ini']

xreds = create_xred(red,
                    read_qd_config(settings_path + f for f in qd_settings))

calculations = read_calculations_config(settings_path + f for f in calc_settings)

for xred in xreds:
    print("{0}:\n \t q_min = {1}, q_max = {2}".format(xred.name, xred.q_min, xred.q_max))

folders_list = []
for key, value in calculations['Calculations'].items():
    if value and key not in folders_list:
        folders_list.append(key)
[os.mkdir(folder) for folder in folders_list if not os.path.isdir(folder)]


img_format = calculations['Other_options']['формат файла']
h = calculations['Method_options']['h']
x_0 = [calculations['Method_options'][ix] for ix in ('W_0', 'Q_0', 'Qe_0')]
tasks = calculations['Calculations']

for xred in xreds:
    print("Вычисления для {} ".format(xred.name))

    pto = " c P_to" if xred.with_pto is True else ""

    if tasks['Решение детерминированное'] is True:
        print("\tрешение детерминированное")
        fig_1, fig_2 = plot.plot_solution(xred, 'ode', h, x_0)

        folder = "Решение детерминированное" + os.path.sep

        img_1 = "{0}{1}_{2} решения{3}.{4}"
        img_1 = img_1.format(folder, xred.name, 'ode', pto, img_format)

        img_2 = "{0}{1}_{2} фазовый портрет{3}.{4}"
        img_2 = img_2.format(folder, xred.name, 'ode', pto, img_format)

        fig_1.savefig(img_1, format=img_format, dpi=300, bbox_inches='tight')
        fig_2.savefig(img_2, format=img_format, dpi=300, bbox_inches='tight')

        del img_1, img_2
        plt.close('all')

    if tasks['Решение стохастическое'] is True:
        print('\tрешение cтохастическое')
        fig_1, fig_2 = plot.plot_solution(xred, 'sde', h, x_0)

        folder = "Решение стохастическое" + os.path.sep

        img_1 = "{0}{1} {2} решения{3}.{4}"
        img_1 = img_1.format(folder, xred.name, 'sde', pto, img_format)

        img_2 = "{0}{1} {2} фазовый портрет{3}.{4}"
        img_2 = img_2.format(folder, xred.name, 'sde', pto, img_format)

        fig_1.savefig(img_1, format=img_format, dpi=300, bbox_inches='tight')
        fig_2.savefig(img_2, format=img_format, dpi=300, bbox_inches='tight')

        del img_1, img_2
        plt.close('all')

    if tasks['Спектр детерминированный'] is True:
        print("\tспектр детерминированный")
        fig = plot.plot_solution_spectrum(xred, 'ode', h, x_0)

        folder = "Спектр детерминированный" + os.path.sep
        img = "{0}{1} спектр{2}.{3}"
        img = img.format(folder, xred.name, pto, img_format)

        fig.savefig(img, format=img_format, dpi=300, bbox_inches='tight')

        del img
        plt.close('all')

    if tasks['Спектр стохастический'] is True:
        print("\tспектр стохастический")
        fig = plot.plot_solution_spectrum(xred, 'sde', h, x_0)

        folder = "Спектр стохастический" + os.path.sep
        img = "{0}{1} спектр{2}.{3}"
        img = img.format(folder, xred.name, pto, img_format)

        fig.savefig(img, format=img_format, dpi=300, bbox_inches='tight')

        plt.close('all')

    if tasks['Автоколебания детерминированные'] is True:
        print("\tавтоколебания детерминированные")

        fig = plot.plot_self_oscillation_solution_2d(xred, 'ode')

        folder = 'Автоколебания детерминированные' + os.path.sep
        img = "{0}{1} 2d{2}.{3}".format(folder, xred.name, pto, img_format)

        fig.savefig(img, format=img_format, dpi=300, bbox_inches='tight')

        file = plot.plot_self_oscillation_solution_3d(xred, 'ode')
        if os.path.isfile(folder + os.path.sep + file):
            os.remove("".join([folder, os.path.sep, file]))
        os.rename(file, folder + os.path.sep + file)

        del img
        plt.close('all')

    if tasks['Автоколебания стохастические'] is True:
        print("\tавтоколебания стохастические")

        fig = plot.plot_self_oscillation_solution_2d(xred, 'sde')

        folder = 'Автоколебания стохастические' + os.path.sep

        img = "{0}{1} 2d{2}.{3}".format(folder, xred.name, pto, img_format)

        fig.savefig(img, format=img_format, dpi=300, bbox_inches='tight')

        file = plot.plot_self_oscillation_solution_3d(xred, 'sde')
        if os.path.isfile(folder + os.path.sep + file):
            os.remove(folder + os.path.sep + file)
        os.rename(file, folder + os.path.sep + file)

        del img
        plt.close('all')
