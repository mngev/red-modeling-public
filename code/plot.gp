#!/usr/bin/gnuplot -persist
# Выбор терминала и формата файла для вывода рисунка
#set terminal png enhanced size 1300,868 font "Arial, 16"
set terminal pdf enhanced size 15,9  font "Arial, 16"
#Установка кодировки
set encoding utf8
# Нарисовать сетку и засечки на осях
set grid xtics ytics
# Оставить только нижнюю и левую границы
set border 3
#Убрать ось x сверху и ось y снизу
set xtics nomirror
set ytics nomirror

# Число ломанных
set samples 5000

set xlabel "q_{min}"
set ylabel "q_{max}"
set zlabel "Q"

#set view map
set hidden3d
## set dgrid3d 30,30
## set view 50,40
set view 50,240
set datafile separator ","
if (!exists("sde")){
	set output model_name."_3d.pdf"
	splot "data.csv" using ($1*100):($2*100):($4) with points pointtype 7 \
    pointsize 0.5 linecolor rgb "black" title "Стационарная точка",  \
    y title 'q=q_{max}'
} else {
	set output model_name."_stochastic_3d.pdf"
	set view 60,240
	splot "data.csv" using ($1*100):($2*100):($4) with points pointtype 7 \
    pointsize 0.5 linecolor rgb "black" title "Стационарная точка",\
    "data.csv" using ($1*100):($2*100):($4-$6) with points pointtype 7 \
    pointsize 0.5 linecolor rgb "green"  title "Вклад стохастического члена",\
    "data.csv" using ($1*100):($2*100):($4+$6) with points pointtype 7 \
    pointsize 0.5 linecolor rgb "red" title "Вклад стохастического члена", \
    y title 'q=q_{max}'
	}