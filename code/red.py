# -*- coding: utf-8 -*-
import numpy as np

# -----------------------------------------------------------------------------
#       Общие функции, не относящиеся к дисциплинам обслуживания
# -----------------------------------------------------------------------------


def ind(x):
    """Функция индикатор"""
    if x > 0:
        return 1.0
    else:
        return 0.0


def matrixind(A):
    """Функция поэлементно обрабатывает массив, возвращая массив на основе
    исходного в котором все отрицательные элементы заменены на нули."""
    return np.abs(0.5*(1.0 + np.sign(A))*A)


# -----------------------------------------------------------------------------
#       RED
# -----------------------------------------------------------------------------
class RED(object):
    """Класс, реализующий дисциплину обслуживания RED. Это суперкласс для всех
    остальных объектов (объектом является дисциплина обслуживания).
    Другие классы переопределяют конструктор '__init__()' и наследуют все
    атрибуты. Обязательно меняют атрибут name на свое имя и при необходимости
    добавляют свои атрибуты и переопределяют методы P, ode и др."""

    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        """Конструктор класса RED.
        Аргументы:
            init_data: словарь с парами атрибутов `{key1: val1, key2: val2 }`,
            q_min, q_max: значения соответствующих атрибутов объекта RED.

        Атрибуты:
            name: имя дисциплины обслуживания,
            Tp: время прохождения пакета от источника до узла и обратно,
            wq: вес очереди,
            w_max: максимальный размер TCP-окна,
            q_min: минимальное пороговое значение пакетов для алгоритма RED %,
            q_max: максимальное пороговое значение пакетов для алгоритма RED %,
            c_small: количество обслуживаемых за 1 секунду пакетов,
            R: размер буфера,
            p_max: максимальная вероятность сброса,
            N: количество узлов,
            delta: вычисляется на основе c_small,
            time_interval: временной интервал интегрирования, кортеж  (t_0, T),
            with_pto: включить/выключить сброс по тайм-ауту.
        """
        self.name = "RED"

        self.q_min = q_min
        self.q_max = q_max
        self.p_max = 0.2

        self.Tp = 0.01
        self.wq = 0.0007
        self.w_max = 32.0
        self.c_small = 1400.0
        self.R = 100.0
        self.N = 1
        self.time_start = 0.0
        self.time_stop = 50.0
        self.with_pto = False

        self.get_atributs_from_dic(init_data)

        self.delta = 1.0/self.c_small
        self.time_interval = (self.time_start, self.time_stop)

    def get_atributs_from_dic(self, init_data):
        """Возможность задавать атрибуты с помощью словаря, но в словаре должны
        быть только допустимые атрибуты, определенные в `__init()__` данный
        метод делает проверку корректности задания атрибутов. Если найден
        незнакомый атрибут, то он игнорируется, а пользователю выдается соот-
        ветствующее сообщение."""
        if init_data is None:
            return 0
        try:
            assert type(init_data) is dict
            pass
        except AssertionError:
            print("{0} должен быть словарем (тип `dict`)".format(init_data))

        for key, value in init_data.items():
            try:
                getattr(self, key)
                setattr(self, key, value)
            except AttributeError:
                print("Параметр {0} недопустим и игнорируется!".format(key))
        return 0

    # вспомогательные функции
    def Ttot(self, x):
        """Время потери по тайм-ауту"""
        return self.Tp + x/self.c_small

    def C(self, x):
        """Интенсивность обслуженной нагрузки"""
        if x >= self.c_small:
            return self.c_small
        else:
            return x

    def p_to(self, x):
        """Вероятность сброса пр тайм-ауту"""
        return min(1.0, 3.0/x)

    # Функции вероятности сброса
    def P(self, x):
        """ Функция вероятности сброса RED применяемая при
        решении дифференциальных уравнений"""
        if x >= self.q_min*self.R and x <= self.q_max*self.R:
            dq = (self.q_max - self.q_min)*self.R
            return self.p_max*(x - self.q_min*self.R)/dq
        elif x < self.q_min*self.R:
            return 0.0
        elif x > self.q_max*self.R:
            return 1.0

    def P2(self, x):
        """ Функция вероятности сброса RED применяемая при
        решении уравнений автоколебания"""
        if x < self.q_min:
            return 0.0
        else:
            dq = (self.q_max - self.q_min)*self.R
            return self.p_max*(x - self.q_min*self.R)/dq

    # Основные функции ОДУ (правая часть ОДУ)
    def W(self, x):
        """Правая часть первого уравнения системы ОДУ. Функции для вычисления
        размера TCP-окна."""
        W = ind(self.w_max - x[0]) / self.Ttot(x[1])
        if not self.with_pto:
            W += ind(x[0] - 1.0)*(x[0]*self.P(x[2])/self.Ttot(x[1]))*(-x[0]/2.0)
        else:
            W += (x[0]*self.P(x[2])/self.Ttot(x[1]))*(-x[0]/2.0)*(1.0 - self.p_to(x[0]))
            W += (1.0 - x[0])*self.P(x[2])*self.p_to(x[0])
        return W

    def W_sde(self, x):
        """Правая часть первого уравнения системы СДУ. Функции для вычисления
        размера TCP-окна.
        Отличие от W в отсутствии знака минус у выражения x[0]/2.0"""
        W = ind(self.w_max - x[0]) / self.Ttot(x[1])
        if not self.with_pto:
            W += ind(x[0] - 1.0)*(x[0]*self.P(x[2])/self.Ttot(x[1]))*(-x[0]/2.0)
        else:
            W += (x[0]*self.P(x[2])/self.Ttot(x[1]))*(x[0]/2.0)*(1.0 - self.p_to(x[0]))
            W += (1.0 - x[0])*self.P(x[2])*self.p_to(x[0])
        return ind(W)

    def Q(self, x):
        """Правая часть уравнения для вычисления мгновенного размера очереди."""
        Q = -self.C(x[1]) + (ind(self.R - x[1]))*(x[0]/self.Ttot(x[1]))*(1.0 - self.P(x[2]))*self.N
        return Q

    def Qe(self, x):
        """Правая часть третьего уравнения системы, вычисляющее экспоненциально
        взвешенное скользящее среднее значение мгновенной длины очереди."""
        return np.log(1.0 - self.wq)*(x[2] - x[1])/self.delta

    # Правая часть системы ОДУ
    def ode(self, t, x, p):
        """Правая часть ОДУ. Три аргумента добавлены для совместимость.

        Аргументы:
            t: время,
            x: переменная массив [W(t), Q(t), Qe(t)],
            p: параметры.
        """
        return [self.W(x), self.Q(x), self.Qe(x)]

    # Правая часть СДУ. Решатель для СДУ требует функцию с одним аргументом,
    # поэтому нельзя воспользоваться методом ode. Кроме того, полезно вернуть
    # массив numpy, а не список python.
    def sde_f(self, x):
        """Вектор сноса"""
        return np.array(self.ode(0.0, x, 0.0))

    def sde_G(self, x):
        """Матрица диффузии (в данном случае она диагональна)"""
        # Чтобы не писать для каждого подкласса RED свою функцию (надо ведь
        # учесть специфику каждой дисциплины обслуживания) мы воспользуемся
        # методом ode, который как раз и переопределяется каждым подклассом
        # суперкласса RED. Этому поможет диагональность матрицы диффузий
        W_tmp = self.W
        Qe_tmp = self.Qe

        # Заменяем детерминированные методы на стохастические
        # а функцию Qe вообще делаем нулевой
        self.W = self.W_sde
        self.Qe = lambda e: 0.0

        # Так как матрица диагональна, то можно обойтись
        # поэлементным извлечением корня. Вдобавок обнуляем все отрицательные
        # элементы
        #G = np.sqrt(matrixind(np.diag(self.ode(0.0, x, 0.0))))
        G = np.sqrt(np.abs(np.diag(self.ode(0.0, x, 0.0))))

        # Возвращаем исходные методы на место
        self.W = W_tmp
        self.Qe = Qe_tmp

        del W_tmp, Qe_tmp

        return G

    # Численное решение ОДУ
    def solve_ode(self, step=0.01, x_0=[1.0, 0.0, 0.0]):
        """Метод решает ОДУ, ассоциированное с дисциплиной обслуживания.
        Для решения используется метод Рунге-Кутты из файла rungekutta.py.

        Аргументы:
            step: шаг,
            x_0: начальные значения в виде списка из трех элементов.
        """
        import rungekutta as rk
        t, y = rk.RK(3).integrate(self.ode, x_0, self.time_interval, step)
        return (t, y[0], y[1], y[2])

    # Численное решение СДУ
    def solve_sde(self, step=0.01, x_0=[1.0, 0.0, 0.0]):
        """Метод решает СДУ, ассоциированное с дисциплиной обслуживания.
        Для решения используется метод из файла sde.py.

        Аргументы:
            step: шаг,
            x_0: начальные значения в виде списка из трех элементов.
        """
        import sde
        N = int((self.time_interval[1] - self.time_interval[0])/step)
        (h, t, dV, V) = sde.wiener_process(N, dim=3, interval=self.time_interval)
        (W, Q, Qe) = sde.strongSRKp1Wm1(self.sde_f, self.sde_G, h, x_0, 3.0*dV).T
        return (t, matrixind(W), Q, Qe)

    def self_oscillation_equation(self, x):
        """Система из двух уравнений для вычисления параметров автоколебания."""

        # Вызываем функцию ode чтобы учесть специфику подклассов, так как она
        # прописана именно в этой функции. Однако для некоторых случаев (POWARED)
        # придется переопределять self_oscillation_equation, так как алгоритм
        # для ОДУ отличается от алгоритма для алгебраических уравнений
        # автоколебания
        y = [x[0], x[1], x[1]]
        P = self.P
        self.P = self.P2
        res = self.ode(0.0, y, 0.0)
        self.P = P
        return res[0:2] # возвращается только W и Q

    def solve_self_oscillation_equation_2d(self, equation_type='ode'):
        """Решает систему алгебраических уравнений для параметров
        автоколебания в случае изменения только q_max."""
        import scipy.optimize

        # Сохраняем предыдущие значения атрибутов, чтобы в конце вернуть назад
        q_max_old, q_min_old = (self.q_max, self.q_min)
        self.q_min = 0.0
        q_max = np.arange(self.q_min + 0.01, 0.99, 0.01)
        W = []
        Q = []

        for self.q_max in q_max:
            x_0 = [self.w_max/8.0, 0.5*(self.q_max + self.q_min)*self.R]
            sol = scipy.optimize.root(self.self_oscillation_equation, x_0, method='hybr')

            W.append(sol.x[0])
            Q.append(sol.x[1])

        # Возвращаем все измененные атрибуты к прежним значениям
        (self.q_max, self.q_min) = (q_max_old, q_min_old)

        return (q_max, W, Q)

    def solve_self_oscillation_equation_3d(self):
        """Решает систему алгебраических уравнений для параметров
        автоколебания в случае изменения и q_max и q_min."""
        import scipy.optimize

        # Сохраняем предыдущие значения атрибутов, чтобы в конце вернуть назад
        q_max_old, q_min_old = (self.q_max, self.q_min)
        self.q_min, self.q_max = (0.0, 0.0)

        q_max = []
        q_min = []
        W = []
        Q = []

        q_max = np.linspace(self.q_min, 1.0, 100)
        q_min = np.linspace(self.q_min, 1.0, 100)
        q_grid = [(q1, q2) for q1 in q_min for q2 in q_max if q1 < q2]

        for self.q_min, self.q_max in q_grid:
            x_0 = [self.w_max/8.0, 0.5*(self.q_max + self.q_min)*self.R]
            sol = scipy.optimize.root(self.self_oscillation_equation, x_0, method='hybr')

            W.append(sol.x[0])
            Q.append(sol.x[1])

        # Возвращаем все атрибуты к прежним значениям
        (self.q_max, self.q_min) = (q_max_old, q_min_old)
        return (q_grid, W, Q)


# -----------------------------------------------------------------------------
#       ARED
# -----------------------------------------------------------------------------
class ARED(RED):
    """Класс, реализующий дисциплину обслуживания ARED"""
    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        super(ARED, self).__init__(init_data, q_min, q_max)
        self.name = "ARED"

    def ode(self, t, x, p):
        """Правая часть ОДУ. Три аргумента добавлены для совместимость.

        Аргументы:
            t: время,
            x: переменная массив [W(t), Q(t), Qe(t)],
            p: параметры.

        ARED параметры, вычисляются на основе RED параметров
        targ_1, targ_2, alpha, beta
        """
        targ_1 = self.q_min + 0.4*(self.q_max - self.q_min)
        targ_2 = self.q_min + 0.6*(self.q_max - self.q_min)
        alpha = min(0.01, self.p_max/4.0)
        beta = 0.9
        # --- Изменение p_max в соответствии с алгоритмом ARED
        if x[2] > targ_2 and self.p_max <= 0.5:
            self.p_max = self.p_max + alpha
        elif x[2] < targ_1 and self.p_max >= 0.01:
            self.p_max = self.p_max*beta
        return [self.W(x), self.Q(x), self.Qe(x)]


# -----------------------------------------------------------------------------
#       POWARED
# -----------------------------------------------------------------------------
class POWARED(RED):
    """Класс, реализующий дисциплину обслуживания POWARED"""
    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        self.K_factor = 2.0
        self.beta = 5.0

        super(POWARED, self).__init__(init_data, q_min, q_max)
        self.name = "POWARED"
        self.Q_target = 0.5*(self.q_min + self.q_max)

    def ode(self, t, x, p):
        """Правая часть ОДУ. Три аргумента добавлены для совместимость.

        Аргументы:
            t: время,
            x: переменная массив [W(t), Q(t), Qe(t)],
            p: параметры.

        Вычисляются:
            α: deviation of q current from q target
            δ: increment or decrement adjustment
            K: power factor ( κ > 1)
            β: compress factor
        """
        alpha = x[1] - self.Q_target*self.R

        if alpha == 0.0:
            powared_delta = 0.0
        elif alpha < 0.0:
            powared_delta = abs((alpha/(self.R*self.Q_target*self.beta))**self.K_factor)
            self.p_max = ind(self.p_max - powared_delta)*(self.p_max - powared_delta)
        elif alpha > 0.0:
            powared_delta = abs((alpha/((self.R - self.Q_target*self.R)*self.beta))**self.K_factor )
            self.p_max = ind(self.p_max + powared_delta)*(self.p_max + powared_delta)
        # ---
        if self.p_max > 1.0:
            self.p_max = 0.99

        return [self.W(x), self.Q(x), self.Qe(x)]

    def self_oscillation_equation(self, x):
        """Система из двух уравнений для вычисления параметров автоколебания
        Для POWARED надо внести ряд изменений, чтобы программа работала"""
        self.Q_target = 0.0
        alpha = x[1] - self.Q_target*self.R

        if alpha == 0.0:
            powared_delta = 0.0
        elif alpha < 0.0:
            powared_delta = abs((alpha/(self.R*self.Q_target*self.beta))**self.K_factor)
            self.p_max = self.p_max - powared_delta
            if self.p_max < 0.0:
                self.p_max = 0.0
        elif alpha > 0.0:
            powared_delta = abs((alpha/((self.R-self.Q_target*self.R)*self.beta))**self.K_factor )
            self.p_max = self.p_max + powared_delta
            if self.p_max > 1.0:
                self.p_max = 1.0
        # ---
        y = [x[0], x[1], x[1]]
        P = self.P
        self.P = self.P2
        res = [self.W(y), self.Q(y)]
        self.P = P
        return res


# -----------------------------------------------------------------------------
#       RARED
# -----------------------------------------------------------------------------
class RARED(RED):
    """Класс, реализующий дисциплину обслуживания RARED"""
    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        super(RARED, self).__init__(init_data, q_min, q_max)
        self.name = "RARED"

    def ode(self, t, x, p):
        """Правая часть ОДУ. Три аргумента добавлены для совместимость.

        Аргументы:
            t: время,
            x: переменная массив [W(t), Q(t), Qe(t)],
            p: параметры.

        Вычисляются:
        alpha, beta, targ_1, targ_2"""

        targ_1 = (self.q_min + 0.48*(self.q_max - self.q_min))*self.R
        targ_2 = (self.q_min + 0.52*(self.q_max - self.q_min))*self.R

        if x[2] > targ_1 and self.p_max <= 0.5:
            alpha = 0.25*(x[2] - targ_1)*self.p_max/targ_1
            self.p_max = self.p_max + alpha
        elif x[2] < targ_2 and self.p_max >= 0.01:
            beta = 1.0 - 0.17*(targ_2 - x[2])/(targ_2 - self.q_min*self.R)
            self.p_max = self.p_max*beta

        return [self.W(x), self.Q(x), self.Qe(x)]

    def self_oscillation_equation(self, x):
        """Система из двух уравнений для вычисления параметров автоколебания
        Для POWARED надо внести ряд изменений, чтобы программа работала"""
        targ_1 = self.q_min + 0.48*(self.q_max - self.q_min)
        targ_2 = self.q_min + 0.52*(self.q_max - self.q_min)
        targ_0 = 0.5*self.R*(targ_1 + targ_2)

        if x[1] > targ_0:
            alpha = 0.25*((x[1] - targ_0)/targ_0)*self.p_max
            self.p_max += alpha
        elif x[1] <= targ_0:
            beta = 1.0 - 0.17*((targ_0 - x[1])/(targ_0 - self.q_min*self.R))
            self.p_max *= beta

        if abs(self.p_max) < 0.01:
            self.p_max = 0.01
        elif abs(self.p_max) > 1.0:
            self.p_max = 1.0

        y = [x[0], x[1], x[1]]
        P = self.P
        self.P = self.P2
        res = [self.W(y), self.Q(y)]
        self.P = P
        return res


# -----------------------------------------------------------------------------
#       AURED
# -----------------------------------------------------------------------------
class AURED(RED):
    """Класс, реализующий дисциплину обслуживания AURED"""
    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        """У AURED есть три новых параметра alpha, beta, d_t"""

        self.alpha = 0.02
        self.beta = 0.8
        self.d_t = 1.0

        super(AURED, self).__init__(init_data, q_min, q_max)
        self.name = "AURED"

    def ode(self, t, x, p):
        """Правая часть ОДУ. Три аргумента добавлены для совместимость.

        Аргументы:
            t: время,
            x: переменная массив [W(t), Q(t), Qe(t)],
            p: параметры."""

        pt = self.Q(x)
        if pt < 1.0:
            if pt < x[1]:
                self.d_t = -self.d_t
            if self.d_t > 0.0:
                self.p_max += self.alpha
            else:
                self.p_max *= self.beta

            if self.p_max > 1.0:
                self.p_max = 1.0
            elif self.p_max <= 0.0:
                self.p_max = 0.01

        return [self.W(x), self.Q(x), self.Qe(x)]


# -----------------------------------------------------------------------------
#       DSRED
# -----------------------------------------------------------------------------
class DSRED(RED):
    """Класс, реализующий дисциплину обслуживания DSRED"""
    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        self.gamma = 0.91
        super(DSRED, self).__init__(init_data, q_min, q_max)
        self.name = "DSRED"

    def P(self, x):
        """Отличительной чертой функции вероятности сброса для
        DSRED`а является добавление среднего значения между
        q_min и q_max"""
        # self.q_mid необходимо здесь пересчитывать! А то q_min и q_max
        # меняются, а q_mid вычисляется только в конструкторе один раз

        self.q_mid = 0.5*(self.q_max + self.q_min)
        dq = (self.q_max - self.q_min)*self.R
        alpha = 2.0*(1.0 - self.gamma)/dq
        beta = 2.0*self.gamma/dq

        if x < self.q_min*self.R:
            return 0.0
        elif self.q_min*self.R <= x < self.q_mid*self.R:
            return alpha*(x - self.q_min*self.R)
        elif self.q_mid*self.R <= x < self.q_max*self.R:
            return 1.0 - self.gamma + beta*(x - self.q_mid*self.R)
        elif x >= self.q_max*self.R:
            return 1.0

    def P2(self, x):
        """Альтернативное вычисление вероятности сброса для случая расчета
        параметров автоколебаний"""
        # self.q_mid необходимо здесь пересчитывать! А то q_min и q_max
        # меняются, а q_mid вычисляется только в конструкторе один раз

        self.q_mid = 0.5*(self.q_max + self.q_min)
        dq = (self.q_max - self.q_min)*self.R
        alpha = 2.0*(1.0 - self.gamma)/dq
        beta = 2.0*self.gamma/dq

        if x < self.q_min*self.R:
            return 0.0
        elif x < self.q_mid*self.R:
            return alpha*(x - self.q_min*self.R)
        elif x <= self.q_max*self.R:
            return abs(1.0 - self.gamma + beta*(x - self.q_mid*self.R))
        else:
            return 1.0


# -----------------------------------------------------------------------------
#       WRED
# -----------------------------------------------------------------------------
class WRED(RED):
    """Класс, реализующий дисциплину обслуживания AURED"""
    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        super(WRED, self).__init__(init_data, q_min, q_max)
        self.name = "WRED"

    def P(self, x):
        """Для алгоритма WRED надо определить свою функцию вычисления
        вероятности"""
        if x >= self.q_min*self.R and x <= self.q_max*self.R:
            dq = (self.q_max-self.q_min)*self.R
            return (self.p_max/(1.0+float(self.N)))*(x-self.q_min*self.R)/dq
        elif x < self.q_min*self.R:
            return 0.0
        elif x > self.q_max*self.R:
            return 1.0

    def P2(self, x):
        """Альтернативное вычисление вероятности сброса для случая расчета
        параметров автоколебаний"""
        if x < self.q_min:
            return 0.0
        else:
            dq = (self.q_max-self.q_min)*self.R
            return (self.p_max/(1.0+float(self.N)))*(x-self.q_min*self.R)/dq

    def ode(self, t, x, p):
        """Правая часть ОДУ. Три аргумента добавлены для совместимость.

        Аргументы:
            t: время,
            x: переменная массив [W(t), Q(t), Qe(t)],
            p: параметры.
        """
        return [self.W(x), self.Q(x), self.Qe(x)]


# -----------------------------------------------------------------------------
#       SARED
# -----------------------------------------------------------------------------
class SARED(RED):
    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        self.W_q_high = 0.0007
        self.W_q_norm = 0.0002

        super(SARED, self).__init__(init_data, q_min, q_max)
        self.name = "SARED"

    def ode(self, t, x, p):
        """Правая часть ОДУ. Три аргумента добавлены для совместимость.

        Аргументы:
            t: время,
            x: переменная массив [W(t), Q(t), Qe(t)],
            p: параметры.
        """
        x_tmp = [0.0, 0.0, 0.0]

        # SARED параметры, вычисляются на основе RED параметров
        target_1 = (self.q_min + 0.4*(self.q_max - self.q_min))*self.R
        target_2 = (self.q_min + 0.6*(self.q_max - self.q_min))*self.R

        # Изменение Qe в соответствии с алгоритмом SARED
        if x[2] >= target_1 and x[2] <= target_2:
            x_tmp[2] = (1.0 - self.W_q_norm)*x[2] + self.W_q_norm*x[1]
        elif x[2] < target_1 or x[2] > target_2:
            x_tmp[2] = (1.0 - self.W_q_high)*x[2] + self.W_q_high*x[1]

        x_tmp = [x[0], x[1], x_tmp[2]]

        return [self.W(x_tmp), self.Q(x_tmp), self.Qe(x_tmp)]


# -----------------------------------------------------------------------------
#       EFRED
# -----------------------------------------------------------------------------
class EFRED(RED):

    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        """
        c: число пакетов со времени первого отброшенного/помеченного
        m: некий коэффициент
        t_q: последний момент времени, когда очередь была пуста
        """
        self.c = 1
        self.m = 1.0
        self.t_q = 0.0

        super(EFRED, self).__init__(init_data, q_min, q_max)
        self.name = "EFRED"

    def P(self, x):
        """Функция вероятности сброса"""
        if self.q_min*self.R < x <= self.q_max*self.R:
            self.c += 1
            dq = (self.q_max-self.q_min)*self.R
            P = self.p_max*(x - self.q_min*self.R)/dq
            P /= (1.0 - float(self.c)*P)
            self.c = 0
        elif x > self.q_max*self.R:
            P = 1.0
            self.c = 0
        elif x < self.q_min*self.R:
            P = 0.0
            self.c = -1
        return P

    def P2(self, x):
        """Альтернативное вычисление вероятности сброса для случая расчета
        параметров автоколебаний"""
        if x < self.q_min:
            self.c = -1
            P = 0.0
        else:
            self.c += 1
            dq = (self.q_max-self.q_min)*self.R
            P = self.p_max*(x - self.q_min*self.R)/dq
            P /= (1.0 - float(self.c)*P)
            self.c = 0
        return P

    def ode(self, t, x, p):
        """Правая часть ОДУ. Три аргумента добавлены для совместимость.

        Аргументы:
            t: время,
            x: переменная массив [W(t), Q(t), Qe(t)],
            p: параметры.
        """
        # Для каждого пакета (а у нас в каждый момент времени 1 пакет)
        if abs(x[1]) > 0.0:  # Если очередь не пуста
            Q_avg = (1.0 - self.wq)*x[2] + self.wq*x[1]
        else:
            # В статье сказано, что m <- f(t-t_q),
            # где f --- линейная функция времени
            self.m = 0.8*(t - self.t_q)
            Q_avg = x[2]*(1.0 - self.wq)**self.m

        if x[1] < 0.0:
            self.t_q = t

        x_tmp = [x[0], x[1], Q_avg]

        return [self.W(x_tmp), self.Q(x_tmp), self.Qe(x_tmp)]
# -----------------------------------------------------------------------------
#       GRED
# -----------------------------------------------------------------------------
class GRED(RED):
    """Класс, реализующий дисциплину обслуживания DSRED"""
    def __init__(self, init_data=None, q_min=0.2, q_max=0.4):
        self.gamma = 0.91
        super(GRED, self).__init__(init_data, q_min, q_max)
        self.name = "GRED"

    def P(self, x):
        """Отличительной чертой функции вероятности сброса для
        GRED`а является добавление тертьего значения 2*q_max. Реализуем
        это как три точки: q_min, q_mid = q_max/2, q_max"""
        # self.q_mid необходимо здесь пересчитывать! А то q_min и q_max
        # меняются, а q_mid вычисляется только в конструкторе один раз

        self.q_mid = 0.5*self.q_max
        dq = (self.q_mid - self.q_min)*self.R

        if x < self.q_min*self.R:
            return 0.0
        elif self.q_min*self.R <= x < self.q_mid*self.R:
            return self.p_max*(x - self.q_min*self.R)/dq
        elif self.q_mid*self.R <= x < self.q_max*self.R:
            p = self.p_max + (1.0 - self.p_max)*(x - self.q_mid)/self.q_mid
            if p < 1.0:
                return p
            else:
                return 0.95
        elif x >= self.q_max*self.R:
            return 1.0

    def P2(self, x):
        """Альтернативное вычисление вероятности сброса для случая расчета
        параметров автоколебаний"""

        self.q_mid = 0.5*self.q_max
        dq = (self.q_mid - self.q_min)*self.R

        if x < self.q_min*self.R:
            return 0.0
        elif self.q_min*self.R <= x < self.q_mid*self.R:
            return self.p_max*(x - self.q_min*self.R)/dq
        elif self.q_mid*self.R <= x < self.q_max*self.R:
            p = self.p_max + (1.0 - self.p_max)*(x - self.q_mid)/self.q_mid
            if p < 1.0:
                return p
            else:
                return 0.95
        elif x >= self.q_max*self.R:
            return 1.0