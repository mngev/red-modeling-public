# compile vanilla ns-2 #

* Download ns-allinone-2.35.tar.gz from <https://sourceforge.net/projects/nsnam/>
* `tar xzvf ns-allinone-2.35.tar.gz`
* `cd ns-allinone-2.35`
* `patch -p1 < ../linkstate.patch`
* `./install`
